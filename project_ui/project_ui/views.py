from django.http import HttpResponse
from django.shortcuts import render
import pandas as pd

def upload_file(request):
    if request.method == 'GET':
        return render(request, 'index.html')
    elif request.method == 'POST':
        data = request.FILES['csv']
        df = pd.read_csv(data)
        
        # Call your FN HERE !!!! in format output_data = fn(args)
        output_data = [[0],[0],[1],[0],[0]]
        input_data = [1.0,0.0,0.0,0.0]
        data_to_send = zip(input_data,output_data)
        data_to_send = list(data_to_send)
        return render(request, 'index.html', {'ip_data':input_data, 'op_data':output_data, 'data_show':data_to_send,'show':True})
    else:
        return HttpResponse(status=404)
        